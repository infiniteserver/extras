package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.text.Text;

@Mixin(net.minecraft.server.network.ServerPlayNetworkHandler.class)
public abstract class ServerPlayNetworkHandlerMixin {
  @Inject(at = @At("HEAD"), method = "disconnect", cancellable = true)
  private void disconnect (Text disconnectReason, CallbackInfo info) {
    info.cancel();
  }
}
