package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;
import com.mojang.brigadier.Command;

@Mixin(net.minecraft.server.dedicated.command.StopCommand.class)
public abstract class StopCommandMixin {
  @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/MinecraftServer;stop(Z)V"), method = "method_13676", cancellable = true)
  private static void stop (CommandContext<ServerCommandSource> context, CallbackInfoReturnable<Integer> info) {
    info.setReturnValue(Command.SINGLE_SUCCESS);
  }
}
