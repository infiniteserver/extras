package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.entity.Entity;

@Mixin(net.minecraft.server.world.ServerWorld.class)
public interface ServerWorldAccessor {
  @Accessor("entitiesById") Int2ObjectMap<Entity> entitiesById ();
}
