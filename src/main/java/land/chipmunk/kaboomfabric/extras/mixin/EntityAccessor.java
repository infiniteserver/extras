package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(net.minecraft.entity.Entity.class)
public interface EntityAccessor {
  @Invoker("destroy") void destroy ();
}
