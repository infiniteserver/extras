package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;
import java.util.Collection;

@Mixin(net.minecraft.server.command.ReloadCommand.class)
public abstract class ReloadCommandMixin {
  @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/MinecraftServer;reload()V"), method = "method_13530", cancellable = true)
  private static void reload (CommandContext<ServerCommandSource> context, CallbackInfoReturnable<Integer> info) {
    info.cancel();
  }
}
