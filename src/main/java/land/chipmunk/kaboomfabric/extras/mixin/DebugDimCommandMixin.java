package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.server.command.ServerCommandSource;

@Mixin(net.minecraft.server.command.DebugDimCommand.class)
public abstract class DebugDimCommandMixin {
  @Inject(at = @At("HEAD"), method = "register", cancellable = true)
  private static void register (CommandDispatcher<ServerCommandSource> dispatcher, CallbackInfo info) {
    info.cancel();
  }
}
