package land.chipmunk.kaboomfabric.extras.mixin;

import land.chipmunk.kaboomfabric.extras.Extras;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import com.mojang.brigadier.CommandDispatcher;

@Mixin(CommandManager.class)
public abstract class CommandManagerMixin {
  @Shadow
  private CommandDispatcher<ServerCommandSource> dispatcher;

  @Inject(at = @At(value = "INVOKE", target = "Lcom/mojang/brigadier/CommandDispatcher;setConsumer(Lcom/mojang/brigadier/ResultConsumer;)V", remap = false), method = "<init>")
  private void init (boolean isDedicatedServer, CallbackInfo info) {
    Extras.registerCommands(dispatcher);
  }
}
