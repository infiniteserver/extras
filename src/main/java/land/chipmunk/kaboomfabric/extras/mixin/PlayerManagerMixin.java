package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.text.Text;
import com.mojang.authlib.GameProfile;
import java.net.SocketAddress;

@Mixin(net.minecraft.server.PlayerManager.class)
public abstract class PlayerManagerMixin {
  @Inject(at = @At("RETURN"), method = "checkCanJoin", cancellable = true)
  private void checkCanJoin (SocketAddress address, GameProfile profile, CallbackInfoReturnable<Text> info) {
    info.setReturnValue(null); // TODO: Config
  }
}
