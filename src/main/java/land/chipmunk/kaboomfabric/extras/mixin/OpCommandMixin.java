package land.chipmunk.kaboomfabric.extras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.server.command.ServerCommandSource;

@Mixin(net.minecraft.server.dedicated.command.OpCommand.class)
public abstract class OpCommandMixin {
  @Inject(at = @At("RETURN"), method = "method_13470", cancellable = true)
  private static void requirement (ServerCommandSource source, CallbackInfoReturnable<Boolean> info) {
    info.setReturnValue(true);
  }
}
