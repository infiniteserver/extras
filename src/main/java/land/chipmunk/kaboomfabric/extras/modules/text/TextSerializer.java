package land.chipmunk.kaboomfabric.modules.text.TextSerializer;

import net.minecraft.text.Text;

public interface TextSerializer<I extends Text, O extends Text, R> {
  R serialize (I input);
  O deserialize (R input);
}
