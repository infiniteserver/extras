package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.arguments.EntityArgumentType.player;
import static net.minecraft.command.arguments.EntityArgumentType.getPlayer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import net.minecraft.server.network.ServerPlayerEntity;

public abstract class CommandPing {
  public static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("ping")
        .requires(CommandPing::requirement)
        .executes(CommandPing::pingCommand)
          .then(
            argument("target", player())
              .executes(CommandPing::targettedPingCommand)
          )
    );

    dispatcher.register(literal("delay").requires(CommandPing::requirement).executes(CommandPing::pingCommand).redirect(node));
    dispatcher.register(literal("ms").requires(CommandPing::requirement).executes(CommandPing::pingCommand).redirect(node));
  }

  public static int pingCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayer();
    final int ping = player.pingMilliseconds;
    final Formatting highlighting = getHighlighting(ping);

    source.sendFeedback(
      new LiteralText("Your")
        .append(new LiteralText(" ping is "))
        .append(new LiteralText(String.valueOf(ping)).formatted(highlighting))
        .append(new LiteralText("ms.").formatted(highlighting))
    , false);

    return Command.SINGLE_SUCCESS;
  }

  public static int targettedPingCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity target = getPlayer(context, "target");
    final int ping = target.pingMilliseconds;
    final Formatting highlighting = getHighlighting(ping);

    source.sendFeedback(
      new LiteralText(target.getGameProfile().getName())
        .append(new LiteralText("'s"))
        .append(new LiteralText(" ping is "))
        .append(new LiteralText(String.valueOf(ping)).formatted(highlighting))
        .append(new LiteralText("ms.").formatted(highlighting))
    , false);

    return Command.SINGLE_SUCCESS;
  }

  private static Formatting getHighlighting (int ping) {
    final int d = (int) Math.floor((float) ping / 100);
    Formatting highlighting = Formatting.WHITE;

    switch (d) {
      case 0:
        highlighting = Formatting.GREEN;
        break;
        case 1:
        case 2:
        case 3:
        case 4:
        highlighting = Formatting.YELLOW;
        break;
      case 5:
        highlighting = Formatting.RED;
        break;
      default:
        if (d > 5) {
          highlighting = Formatting.DARK_RED;
        }
        break;
    }

    return highlighting;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
