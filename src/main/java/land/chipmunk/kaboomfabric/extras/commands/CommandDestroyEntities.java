package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.text.LiteralText;
import land.chipmunk.kaboomfabric.extras.mixin.ServerWorldAccessor;
import land.chipmunk.kaboomfabric.extras.mixin.EntityAccessor;

// Currently broken
public interface CommandDestroyEntities {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("destroyentities")
        .requires(CommandDestroyEntities::requirement)
        // .executes(CommandDestroyEntities::destroyEntitiesCommand)
    );

    dispatcher.register(literal("de").requires(CommandDestroyEntities::requirement).redirect(node));
  }

  static int destroyEntitiesCommand (CommandContext<ServerCommandSource> context) {
    final ServerCommandSource source = context.getSource();
    final MinecraftServer server = source.getMinecraftServer();

    int entityCount = 0;
    int worldCount = 0;
    for (ServerWorld world : server.getWorlds()) {
      for (Entity entity : ((ServerWorldAccessor)(Object)world).entitiesById().values()) {
        if (entity instanceof PlayerEntity) continue;
        try {
          ((EntityAccessor)(Object)entity).destroy();
          entityCount++;
        } catch (Exception ignored) {
        }
      }
      worldCount++;
    }

    source.sendFeedback(
      new LiteralText("Successfully destroyed ")
        .append(new LiteralText(String.valueOf(entityCount)))
        .append(new LiteralText(" entities in "))
        .append(new LiteralText(String.valueOf(worldCount)))
        .append(new LiteralText(" worlds")),
    true);

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
