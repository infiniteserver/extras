package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static net.minecraft.server.command.CommandManager.literal;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;

public abstract class CommandEnchantAll {
  private static final SimpleCommandExceptionType EMPTY_ITEM_EXCEPTION = new SimpleCommandExceptionType(new LiteralText("Please hold an item in your hand to enchant it"));

  public static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("enchantall")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(CommandEnchantAll::enchantAllCommand)
    );
  }

  public static int enchantAllCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayer();
    final PlayerInventory inventory = player.inventory;

    final ListTag enchantments = new ListTag();
    for (Identifier identifier : Registry.ENCHANTMENT.getIds()) {
      final CompoundTag enchantment = new CompoundTag();
      enchantment.putString("id", identifier.toString());
      enchantment.putShort("lvl", (short) 0xFF);
      enchantments.add(enchantment);
    }

    final ItemStack stack = inventory.getStack(inventory.selectedSlot).copy();
    if (stack.isEmpty()) throw EMPTY_ITEM_EXCEPTION.create();

    CompoundTag nbt = stack.getTag();
    if (nbt == null) {
      nbt = new CompoundTag();
      stack.setTag(nbt);
    }
    stack.getTag().put("Enchantments", enchantments);
    inventory.setStack(inventory.selectedSlot, stack);

    source.sendFeedback(new LiteralText("I killed Martin."), false);

    return Command.SINGLE_SUCCESS;
  }
}
