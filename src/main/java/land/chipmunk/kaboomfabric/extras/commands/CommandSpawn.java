package land.chipmunk.kaboomfabric.extras.commands;

import land.chipmunk.kaboomfabric.extras.Extras;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.level.LevelProperties;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.text.LiteralText;
import java.util.Set;
import java.util.HashSet;

public interface CommandSpawn {
  static void register (CommandDispatcher dispatcher) {
    dispatcher.register(
      literal("spawn")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(CommandSpawn::spawnCommand)
    );
  }

  static int spawnCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Entity entity = source.getEntityOrThrow();
    final ServerWorld world = source.getMinecraftServer().getWorld(DimensionType.OVERWORLD);
    final LevelProperties properties = world.getLevelProperties();

    entity.setWorld(world);
    entity.teleport(properties.getSpawnX() + 0.5, properties.getSpawnY(), properties.getSpawnZ() + 0.5);

    source.sendFeedback(new LiteralText("Successfully moved to spawn"), false);

    return Command.SINGLE_SUCCESS;
  }
}
