package land.chipmunk.kaboomfabric.extras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.arguments.EntityArgumentType.players;
import static net.minecraft.command.arguments.EntityArgumentType.getPlayers;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.Identifier;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundCategory;
import net.minecraft.util.math.Vec3d;
import net.minecraft.text.LiteralText;
import java.util.Collection;

public interface CommandJumpscare {
  static void register (CommandDispatcher dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("jumpscare")
        .requires(CommandJumpscare::requirement)
        .then(
          argument("targets", players())
            .executes(CommandJumpscare::jumpscareCommand)
        )
    );

    dispatcher.register(literal("scare").requires(CommandJumpscare::requirement).redirect(node));
  }

  static int jumpscareCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Collection<ServerPlayerEntity> players = getPlayers(context, "targets");
    final ParticleEffect particle = (ParticleEffect) Registry.PARTICLE_TYPE.get(new Identifier("minecraft", "elder_guardian"));
    final SoundEvent soundEvent = Registry.SOUND_EVENT.get(new Identifier("minecraft", "entity.enderman.scream"));

    for (ServerPlayerEntity player : players) {
      final ServerWorld world = player.getServerWorld();
      final Vec3d position = player.getPos();
      world.<ParticleEffect>spawnParticles(player, particle, false, position.getX(), position.getY(), position.getZ(), 4, 0, 0, 0, 1);

      for (int i = 0; i < 10; i++) player.playSound(soundEvent, SoundCategory.MASTER, 1, 0);
    }

    if (players.size() == 1) {
      final ServerPlayerEntity player = (ServerPlayerEntity) players.toArray()[0];

      source.sendFeedback(
        new LiteralText("Successfully created jumpscare for player \"")
          .append(new LiteralText(player.getGameProfile().getName()))
          .append(new LiteralText("\""))
      , true);

      return Command.SINGLE_SUCCESS;
    }

    source.sendFeedback(new LiteralText("Successfully created jumpscare for multiple players"), true);

    return Command.SINGLE_SUCCESS;
  }

  static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }
}
